package com.experiment.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONArray;

import com.experiment.beans.PersonBean;
import com.experiment.database.DataAccess;
import com.google.gson.Gson;

/**
 * Servlet implementation class AjaxServlet
 */
@WebServlet("/AjaxJsonServlet")
public class AjaxJsonServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AjaxJsonServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    private void personList (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
    	PrintWriter out = response.getWriter();
		DataAccess db = new DataAccess();
		db.initConnection();
		
		Gson gson = new Gson();
		JSONArray json = new JSONArray();
		ArrayList<PersonBean> personList = db.getPersonByName(request.getParameter("term"));
		for(int i = 0; i<personList.size(); i++) {
			PersonBean bean = personList.get(i);
			json.add(gson.toJson(bean));
		}

		out.print(json.toJSONString());
		out.close();
    }

    private void transit (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
    	PrintWriter out = response.getWriter();
		DataAccess db = new DataAccess();
		db.initConnection();
		
		Gson gson = new Gson();
		JSONArray json = new JSONArray();
		ArrayList<PersonBean> personList = db.getPersonByName(request.getParameter("term"));
		for(int i = 0; i<personList.size(); i++) {
			PersonBean bean = personList.get(i);
			json.add(gson.toJson(bean));
		}

		out.print(json.toJSONString());
		out.close();
    }
    
    private void host_list (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
    	PrintWriter out = response.getWriter();
		DataAccess db = new DataAccess();
		db.initConnection();
		
		Gson gson = new Gson();
		JSONArray json = new JSONArray();
		ArrayList<PersonBean> personList = db.getPersonByName(request.getParameter("term"));
		for(int i = 0; i<personList.size(); i++) {
			PersonBean bean = personList.get(i);
			json.add(gson.toJson(bean));
		}

		out.print(json.toJSONString());
		out.close();
    }
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getParameter("type").equals("personList")) {
			personList(request, response);
		}
		if(request.getParameter("type").equals("transit")) {
			transit(request, response);
		}
		if(request.getParameter("type").equals("host_list")) {
			host_list(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		methodNameHere(request, response);
	}

}
