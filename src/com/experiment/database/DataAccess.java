package com.experiment.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.experiment.beans.PersonBean;

public class DataAccess {
	public static final String MYSQL_USERNAME = "root";
    public static final String MYSQL_PASSWORD = "";
    public static final String MYSQL_DATABASE_HOST = "localhost";
    public static final String MYSQL_DATABASE_PORT = "3306";
    public static final String MYSQL_DATABASE_NAME = "experiment";
    public static final String MYSQL_DATABASE_DRIVER = "com.mysql.jdbc.Driver";
    private static Connection con;
    /**
     * returns connection to DB
     * @param con Connection
     */
    public static String initConnection(){
        String url = "";
        //if(con==null)con = ServletListener.getConnection();

        try {
            Class.forName(MYSQL_DATABASE_DRIVER).newInstance();
        } catch (InstantiationException e) {
        	e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            
        } catch (ClassNotFoundException e) {
            e.printStackTrace();    //Can't load driver
        }

        if(con==null){
            try {
                url = "jdbc:mysql://" + MYSQL_DATABASE_HOST + ":" + MYSQL_DATABASE_PORT + "/" + MYSQL_DATABASE_NAME;
                con = DriverManager.getConnection(url, MYSQL_USERNAME, MYSQL_PASSWORD);
            } catch (SQLException e) {
                e.printStackTrace();
                return e.getMessage();
            }
        }
        return "Connection Initialized!";
    }
 
    public static Connection getConnection() {
        if (con == null) {
            initConnection();
        }
        return con;
    }
    
	public ArrayList<PersonBean> getPersonByName(String term){
		ArrayList<PersonBean> personList = new ArrayList<PersonBean>();
		
		java.sql.Statement stm = null;
		ResultSet res = null;
		
		try{			
			String selectSql = "SELECT * FROM names WHERE firstname LIKE '%"+term+"%'";
			stm = con.createStatement();
			res = stm.executeQuery(selectSql);
			
			while(res.next()){
				// Get values from the database and add it to orderList
				PersonBean person = new PersonBean(
						res.getInt("id"), 
						res.getString("firstname"), 
						res.getString("lastname"), 
						res.getString("phonenumber"));
				personList.add(person);
			}
			return personList;
		} catch (SQLException e) {
			System.err.println(e.getMessage()+"SQL State: " + e.getSQLState());
			e.printStackTrace();
		}
		return personList;
	}
}
