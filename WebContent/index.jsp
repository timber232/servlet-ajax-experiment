<%@ page language="java" contentType="text/html; charset=US-ASCII"
    pageEncoding="US-ASCII"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
<title>Ajax Testing</title>
<!-- Bootstrap CSS -->
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/lib/bootstrap/css/bootstrap.min.css">
<!-- JQuery UI CSS -->
<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/themes/smoothness/jquery-ui.css" />
<!-- Main -->
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/css/main.css">

<!-- JQuery Script -->
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<!-- JQuery UI Script -->
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
<!-- Bootstrap Script -->
<script type="text/javascript" src="${pageContext.request.contextPath}/assets/lib/bootstrap/js/bootstrap.min.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/ajax_autocomplete.js"></script>

<!-- In-line JavaScript  -->
<script type="text/javascript">
$(document).ready(function() {
	
    // ---------------- LIVE CALCULATION ----------------
    
    // Store the final cost element
    var final_cost_element = $("#finalCost");
    
    // Initialize the variables with user inputs (for the back button)
    var discount = $("input[name=discountRadios]:checked", "#form_test_calculation").attr('data-percent') || 0;
    var estimated_value = $("#estimatedValue").val() || 0;
    var misc_cost = $("#miscCost").val() || 0;
    
    print_debug(estimated_value, misc_cost, discount);
    var total = calculate_total(estimated_value, misc_cost, discount);
	final_cost_element.val(total);
	
	// On change
    $('#form_test_calculation input').change(function(event) {
    	switch ($(this).attr('id')) {
	        case "estimatedValue":
	        	estimated_value = $(this).val(); break;
	        case "miscCost":
	        	misc_cost = $(this).val(); break;
	        default: 
	        	if($(this).is(":radio") && $(this).is(":checked"))
	        		discount = $(this).attr('data-percent');
	        	break;
		}
    	print_debug(estimated_value, misc_cost, discount);
    	var total = calculate_total(estimated_value, misc_cost, discount);
    	final_cost_element.val(total);
    });
    
    function calculate_total(estimated_value, misc_cost, discount) {
    	var cost = parseFloat(estimated_value) + parseFloat(misc_cost);
    	cost = parseFloat(cost).toFixed(2);
    	return parseFloat(cost - (cost * (discount/100))).toFixed(2);
    }
    
    function print_debug(estimated_value, misc_cost, discount) {
    	var cost = parseFloat(estimated_value) + parseFloat(misc_cost);
    	cost = parseFloat(cost).toFixed(2);
    	$('#debug_element').html(
    			"Cost = " + estimated_value + " + " +  misc_cost + "; </br>" +
    			"Cost = " + cost +  "; </br>" +
    			"Final Cost = " + cost + " - " + "("+ cost + "*" + discount + "/100)" + "; </br>" +
    			"Final Cost = " + calculate_total(estimated_value, misc_cost, discount)
		);
    }
});
</script>
</head>
<body>
<!-- Ajax Auto-complete Testing-->
<div class="center-block">
	<h1 class="text-center">Testing</h1>
	<hr>
	<form class="form-horizontal" id="form_test_ajax" method="post" action="TestFormServlet">
			<div class="control-group">
			<label class="control-label" for="inputName">Name</label>
			<div class="controls">
			  <input type="text" name="name" id="inputName" placeholder="Name">
			  <span class="help-block" id="not-found-error"></span>
			  <input type="hidden" name="person_id" id="personId">
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="inputPhone">Phone Number</label>
			<div class="controls">
			  <input type="text" name="phonenumber" id="inputPhone" placeholder="Phone Number">
			</div>
		</div>
		<div class="control-group">
			<div class="controls">
				<button type="submit" name="test" class="btn">Test Me</button>
			</div>
		</div>
	</form>
</div>

<hr>

<!-- JavaScript Live Calculation-->
<div class="center-block" >
	<form class="form-horizontal" id="form_test_calculation" method="post" action="TestFormServlet">
	
		<!-- Discount Radios -->
		<div class="control-group">
			<div class="controls">
				<label class="radio">
					<input type="radio" name="discountRadios" id="discount5" data-percent="5" value="1">5%
				</label>
				<label class="radio">
					<input type="radio" name="discountRadios" id="discount10" data-percent="10" value="2">10%
				</label>
			</div>
		</div>
		
		<!-- Estimated Value -->
		<div class="control-group">
			<label class="control-label" for="estimatedValue">Estimated Value</label>
			<div class="controls">
				<input type="number" name="estimatedValue" id="estimatedValue" placeholder="0.00" step="0.10">
			</div>
		</div>
		
		<!-- Miscellaneous Cost -->
		<div class="control-group">
			<label class="control-label" for="miscCost">Miscellaneous Cost</label>
			<div class="controls">
				<input type="number" name="miscCost" id="miscCost" placeholder="0.00" step="0.10">
			</div>
		</div>
		
		<!-- Final Cost -->
		<div class="control-group">
			<label class="control-label" for="finalCost">Final Cost</label>
			<div class="controls">
				<input type="text" name="finalCost" id="finalCost" placeholder="0.00" disabled>
			</div>
		</div>
		
		<div class="control-group">
			<div class="controls">
				<button type="submit" name="test2" class="btn">Test Me Too</button>
			</div>
		</div>
	</form>
</div>

<hr>

<div class="center-block">
	<div class="well well-small" id="debug_element">
	  
	</div>
</div>

</body>
</html>