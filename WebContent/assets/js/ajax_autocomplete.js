$(document).ready(function(event) {
	$("#inputName").focus(function(event) {
		$('#not-found-error').empty();
	});
	
	$('#inputPhone').val(sessionStorage.getItem("ajax_auto_phone_value") || '');
	$("#inputName").val(sessionStorage.getItem("ajax_auto_label") || '');
	$('#personId').val(sessionStorage.getItem("ajax_auto_id") || '');
	
	$("#inputName").autocomplete({
		minLength: 2,
		// Fetching the data from the servlet
		source: function(request, response) {
			$.ajax({
				type: "get",
				url: 'AjaxJsonServlet?type=personList',
				data: request, // The characters that user entered
				success: function(data) {
					// The raw data is in string, need to conver it to JSON objects
					var parsedJsonShell = $.parseJSON(data);
					response($.map(parsedJsonShell, function(item) {
						// Even though we've already parsed the json string into objects,
						// the keys are still in string. 
						var parsed_item = $.parseJSON(item);
						// Populate the phone number
						$('#inputPhone').val(parsed_item.phonenumber);
						return {
							label: parsed_item.firstname,
							value: parsed_item.id,
							phone_number: parsed_item.phonenumber
						};
					})); // END of response
				}
			}); // END of $.ajax
		},
		response: function(event, ui) {
            if (ui.content.length === 0) {
            	$('#inputPhone, #personId').empty();
            	$('#not-found-error').text("Not found");
            	sessionStorage.removeItem('ajax_auto_id');
            	sessionStorage.removeItem('ajax_auto_label');
            	sessionStorage.removeItem('ajax_auto_phone_value');
            } else {
            	$('#not-found-error').empty();
            }
            return false;
        },
		focus: function(event, ui) {
			// Everytime the user scroll through the dropdown (either by keyboard or mouse)
			// repopulate the text field and the hidden input
			setInputFromSessionStorage(event, ui);
			return false;
		},
		select: function(event, ui) {
			// Everytime the selects an item (either by keyboard or mouse)
			// repopulate the text field and the hidden input
			setInputFromSessionStorage(event, ui);
			return false;
		}
    }); // END of autocomplete
	
	function setInputFromSessionStorage(event, ui) {
		$('#inputPhone').val(sessionStorage.getItem("ajax_auto_phone_value") || ui.item.phone_number);
		$("#inputName").val(sessionStorage.getItem("ajax_auto_label") || ui.item.label);
		$('#personId').val(sessionStorage.getItem("ajax_auto_id") || ui.item.value);
    	sessionStorage.setItem('ajax_auto_id', ui.item.value);
    	sessionStorage.setItem('ajax_auto_label', ui.item.label);
    	sessionStorage.setItem('ajax_auto_phone_value', ui.item.phone_number);
	}
});